import mysql from 'mysql2';

var conexion = mysql.createConnection({
    host:"127.0.0.1",
    user:"root",
    password:"123",
    database:"sistemas"
});

conexion.connect(function (error){
    if(error){
        console.error('Conexión Fallida');
        return;
    } else{
        console.log('Conexión Exitosa')
    }
});

export default conexion;
